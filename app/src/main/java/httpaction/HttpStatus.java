package httpaction;

public interface HttpStatus {
    int GET    = 1;
    int POST   = 2;
    int PUT    = 3;
    int DELETE = 4;
}