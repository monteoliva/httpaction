package httpaction;

public interface HttpCallBack {
    void onResponse(HttpResponse response);
    void onError(HttpResponse response);
}