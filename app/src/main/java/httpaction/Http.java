package httpaction;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Http {
    private Map<String, String> params;
    private Map<String, String> paramsH;

    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final int SIXTY_SECONDS = 60000;

    private int TYPE = HttpStatus.POST;

    Http(Map<String, String> paramsH, Map<String, String> params) {
        Map<String, String> paramsLocal  = new HashMap<>();
        Map<String, String> paramsHLocal = new HashMap<>();

        this.params  = (params  != null) ? params  : paramsLocal;
        this.paramsH = (paramsH != null) ? paramsH : paramsHLocal;
    }

    protected HttpResponse sendGet(@NonNull String urlToRead) {
        TYPE = HttpStatus.GET;
        return send(urlToRead, null);
    }
    protected HttpResponse sendPost(@NonNull String urlToRead, JSONObject jsonBody) {
        TYPE = HttpStatus.POST;
        return send(urlToRead, jsonBody);
    }

    /**
     * Metodo que retorna o String de uma URL
     *
     * @param urlToRead
     * @return
     */
    private HttpResponse send(@NonNull String urlToRead, JSONObject jsonBody) {
        // initialize http result
        HttpResponse result = null;

        // buffer read
        BufferedReader rd;

        final long start = System.currentTimeMillis();

        String connResponseMessage = "";

        try {
            // inicia
            String line;
            String urlParameters = "";
            String requestMethod = "";

            // verifica os params
            for (String key : params.keySet()) {
                // pega a linha
                final String linha = key + "=" + params.get(key);

                // verifica
                if (urlParameters.length() > 0) { urlParameters += "&" + linha; }
                else                            { urlParameters += "?" + linha; }
            }

            // incrementa a URL
            if (!urlParameters.isEmpty()) { urlToRead += urlParameters; }

            // seta a URL
            URL url = new URL(urlToRead);

            switch (TYPE) {
                case HttpStatus.GET:
                    requestMethod = "GET";
                    break;
                case HttpStatus.POST:
                    requestMethod = "POST";
                    break;
                case HttpStatus.PUT:
                    requestMethod = "PUT";
                    break;
                case HttpStatus.DELETE:
                    requestMethod = "DELETE";
                    break;
            }

            // realiza a conexao
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                              conn.setRequestMethod(requestMethod);
                              conn.setDoInput(true);
                              conn.setUseCaches(false);
                              conn.setReadTimeout(SIXTY_SECONDS);
                              conn.setConnectTimeout(SIXTY_SECONDS);

            if (TYPE == HttpStatus.POST) { conn.setDoOutput(true); }

            // verifica os headers
            for (String key : paramsH.keySet()) {
                // seta Header
                conn.setRequestProperty(key, paramsH.get(key));
            }

            if (jsonBody != null) {
                // send data
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

                // JSON Object
                if (jsonBody != null) {
                    wr.write(jsonBody.toString().getBytes(DEFAULT_CHARSET));
                }

                // flush send data
                wr.flush();
                wr.close();
            }

            // initialize InputStream
            InputStream input;

            // response
            final int codeStatus = conn.getResponseCode();
            connResponseMessage  = conn.getResponseMessage();

            // verify status
            if ((codeStatus == HttpURLConnection.HTTP_OK) ||
                (codeStatus == HttpURLConnection.HTTP_CREATED)) {
                // get InputStream
                input = conn.getInputStream();
            }
            else {
                input = conn.getErrorStream();
            }

            // seta o BufferReader
            rd = new BufferedReader(new InputStreamReader(input, DEFAULT_CHARSET));

            // message
            String message = "";

            // percorre o arquivo e retorna o result
            while ((line = rd.readLine()) != null) { message += line; }

            // set Result
            result = new HttpResponse();
            result.setCodeHttp(codeStatus);
            result.setCodeError(0);
            result.setMessage(message);
            result.setStatus("OK");

            // fecha o BufferedReader
            rd.close();
            conn.disconnect();
        }
        catch (MalformedURLException        me) { connResponseMessage = "MalformedURLException: " + me.getMessage(); }
        catch (UnsupportedEncodingException ue) { connResponseMessage = "UnsupportedEncodingException: " + ue.getMessage();  }
        catch (IOException                  ie) {
            if (ie instanceof SocketTimeoutException) {
                result = new HttpResponse();
                result.setCodeHttp(HttpURLConnection.HTTP_CLIENT_TIMEOUT);
                result.setCodeError(99);
                result.setMessage("");
                result.setStatus("ERROR");
            }
            else {
                connResponseMessage = "IOException: " + ie.getMessage();
            }
        }

        if (result == null) {
            result = new HttpResponse();
            result.setCodeHttp(400);
            result.setCodeError(99);
            result.setMessage("");
            result.setStatus("ERROR");
        }

        // calcula o tempo em milesegundos
        final long finish = System.currentTimeMillis();
        final long total  = finish - start;

        // set Response
        result.setTimeFinish(total);
        result.setMessageResponse(connResponseMessage);

        // retorna
        return result;
    }

    /**
     * Class Builder (Design Patterns)
     */
    protected static class Builder {
        private Map<String, String> headerParams;
        private Map<String, String> requestParams;

        public Builder headerParams(Map<String, String> paramsH) {
            this.headerParams = paramsH;
            return this;
        }

        public Builder requestParams(Map<String, String> requestParams) {
            this.requestParams = requestParams;
            return this;
        }

        public Http build() {
            return new Http(headerParams, requestParams);
        }
    }
}