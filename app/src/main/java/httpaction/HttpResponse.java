package httpaction;

// imports JAVA API
import java.io.Serializable;

public class HttpResponse implements Serializable {
    private int codeHttp;
    private int codeError;
    private String message;
    private String status;
    private long timeFinish;
    private String messageResponse;

    public int getCodeHttp()           { return codeHttp;        }
    public int getCodeError()          { return codeError;       }
    public String getMessage()         { return message;         }
    public String getStatus()          { return status;          }
    public long getTimeFinish()        { return timeFinish;      }
    public String getMessageResponse() { return messageResponse; }

    /**
     * Methods Setter´s
     */
    public void setCodeHttp(int codeHttp)      { this.codeHttp        = codeHttp;   }
    public void setCodeError(int codeError)    { this.codeError       = codeError;  }
    public void setMessage(String message)     { this.message         = message;    }
    public void setStatus(String status)       { this.status          = status;     }
    public void setTimeFinish(long timeFinish) { this.timeFinish      = timeFinish; }
    public void setMessageResponse(String msg) { this.messageResponse = msg;        }
}