package httpaction;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.Map;

public class HttpTask extends AsyncTask<String, Void, HttpResponse> {
    private int method;
    private HttpCallBack callBack;
    private Context context;
    private Map<String, String> headerParams;
    private Map<String, String> requestParams;
    private JSONObject jsonBody;

    protected HttpTask(@NonNull Context context,
                       @NonNull int method,
                       Map<String, String> headerParams,
                       Map<String, String> requestParams,
                       JSONObject jsonBody,
                       @NonNull HttpCallBack callBack) {
        this.context       = context;
        this.method        = method;
        this.callBack      = callBack;
        this.headerParams  = headerParams;
        this.requestParams = requestParams;
        this.jsonBody      = jsonBody;
    }

    @Override
    protected HttpResponse doInBackground(String... params) {
        final String sendUrl = params[0] + params[1];

        HttpResponse response = null;
        Http http = new Http.Builder()
                .headerParams(headerParams)
                .requestParams(requestParams)
                .build();

        switch (method) {
            case HttpStatus.GET:
                response = http.sendGet(sendUrl);
                break;
            case HttpStatus.POST:
                response = http.sendPost(sendUrl, jsonBody);
                break;
            case HttpStatus.PUT:
                break;
            case HttpStatus.DELETE:
                break;
        }

        return response;
    }

    @Override
    protected void onPostExecute(HttpResponse response) {
        if (callBack != null && response != null) {
            // get code http
            final int codeHttp = response.getCodeHttp();

            // verify http code
            if ((codeHttp == HttpURLConnection.HTTP_OK) ||
                    (codeHttp == HttpURLConnection.HTTP_CREATED)) {
                // verify message
                if (response.getStatus().equals("ERROR")) { callBack.onError(response);    }
                else                                      { callBack.onResponse(response); }
            }
            else {
                // verifica erro de TimeOut
                if (codeHttp == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {
                    response.setMessage(context.getString(R.string.conection_error));
                }

                // redireciona pra error
                callBack.onError(response);
            }
        }
        else {
            callBack.onError(response);
        }
    }
}