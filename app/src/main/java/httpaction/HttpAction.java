package httpaction;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.Map;

public final class HttpAction {
    final String baseUrl;
    final Context context;

    /**
     * Constructor
     */
    HttpAction(@NonNull Context context, @NonNull String baseUrl) {
        this.context = context;
        this.baseUrl = baseUrl;
    }

    /**
     * Method to Send GET
     *
     * @param urlSend
     * @param requestParams
     * @param callBack
     */
    public void sendGet(@NonNull String urlSend,
                        @NonNull Map<String, String> requestParams,
                        @NonNull final HttpCallBack callBack) {

        if (!isInternetConnect(context)) {
            callBack.onResponse(errorResponse());
            return;
        }

        new HttpTask(context,
                HttpStatus.GET,
                null,
                requestParams,
                null,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse response) {
                        callBack.onResponse(response);
                    }

                    @Override
                    public void onError(HttpResponse response) {
                        callBack.onError(response);
                    }
                }).execute(baseUrl, urlSend);
    }

    /**
     * Method to Send POST
     *
     * @param urlSend
     * @param headerParams
     * @param requestParams
     * @param callBack
     */
    public void sendPost(@NonNull String urlSend,
                         Map<String, String> headerParams,
                         Map<String, String> requestParams,
                         JSONObject jsonBody,
                         @NonNull final HttpCallBack callBack) {

        if (!isInternetConnect(context)) {
            callBack.onResponse(errorResponse());
            return;
        }

        new HttpTask(context,
                HttpStatus.POST,
                headerParams,
                requestParams,
                jsonBody,
                new HttpCallBack() {
                    @Override
                    public void onResponse(HttpResponse response) {
                        callBack.onResponse(response);
                    }

                    @Override
                    public void onError(HttpResponse response) {
                        callBack.onError(response);
                    }
                }).execute(baseUrl, urlSend);
    }

    private HttpResponse errorResponse() {
        HttpResponse response = new HttpResponse();
        response.setCodeHttp(HttpURLConnection.HTTP_BAD_REQUEST);
        response.setCodeError(99);
        response.setMessage("Internet Error");
        response.setStatus("Error");
        return response;
    }

    private boolean isInternetConnect(Context ctx) {
        try {
            // pega a conexao
            final ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

            // pega a conexao ativa
            final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

            // retorna se esta ou nao conectado
            return activeNetwork.isConnected();
        }
        catch (Exception e) { return false; }
    }





    /**
     * Class Builder (Design Patterns)
     */
    public static class Builder {
        private String baseUrl;
        private Context context;

        public Builder baseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public HttpAction build() {
            if (baseUrl == null) {
                throw new IllegalStateException("Base URL required.");
            }
            else if (context == null) {
                throw new IllegalStateException("Context required.");
            }
            return new HttpAction(context, baseUrl);
        }
    }
}